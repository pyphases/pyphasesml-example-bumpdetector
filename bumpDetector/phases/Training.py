from pathlib import Path

from pyPhases import Phase
from pyPhasesML import DatasetWrapXY, ModelManager, TrainingSet


class Training(Phase):
    def prepareConfig(self):
        # the input shape can be calculated from the dataset config values
        datasetConfig = self.getConfig("dataset")
        shape = (datasetConfig["segmentlength"], 1)

        self.log("Set the inputshape to %s" % str(shape))
        self.setConfig("inputShape", shape)

    def createLogFolder(self, path):
        if not Path(path).exists():
            Path(path).mkdir(parents=True, exist_ok=True)
        # you can throw an error if you want to prevent overwriting you training

    def main(self):
        model = ModelManager.getModel()

        # log everything in a specific folder derived from config values
        modelConfigString = self.project.getDataFromName("modelState").getTagString()
        logPath = "logs/%s/" % modelConfigString
        self.createLogFolder(logPath)
        model.logPath = logPath

        # load the trainingsdata from filesystem or generate it
        train = self.project.getData("dataset-training", tuple)
        val = self.project.getData("dataset-validation", tuple)

        trainingsSet = TrainingSet(
            trainingData=DatasetWrapXY(train, batchSize=model.batchSize),
            validationData=DatasetWrapXY(val, batchSize=model.batchSize),
        )
        # model.summary()
        # train the model
        trainedModel = model.train(trainingsSet)

        # save the model state and relevant config values
        self.project.registerData("modelState", trainedModel)
        self.project.saveConfig(logPath + "model.config", "modelState")
