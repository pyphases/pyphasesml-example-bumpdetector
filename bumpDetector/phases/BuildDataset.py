import numpy as np
import random

from pyPhases import Phase


class BuildDataset(Phase):
    """
    in this phase a dummy dataset is generated.
    """

    def generateBumpSignal(self, segmentlength, count, noise=0.2, bumpLikelihood=0.5):
        """this method generates a noised segments with bumps in it."""
        shape = (count, segmentlength)

        x = np.random.rand(*shape)
        x = np.random.normal(x, noise * x) * noise
        x = x.astype(np.float32)
        y = np.full((count, segmentlength), 0)

        bumpLength = [10, 50]

        for i in range(count):
            hasBump = random.random() < bumpLikelihood
            if hasBump:
                length = random.randrange(bumpLength[0], bumpLength[1])
                start = random.randrange(0, segmentlength - length)
                x[i][start : (start + length)] += 1
                y[i][start : (start + length)] = 1

        return x.reshape(count, segmentlength, 1), y

    def buildBySplit(self, split):
        datasetConfig = self.getConfig("dataset")
        return self.generateBumpSignal(
            datasetConfig["segmentlength"],
            split,
            noise=datasetConfig["noise"],
            bumpLikelihood=datasetConfig["bumpLikelyhood"],
        )

    def main(self):
        datasetConfig = self.getConfig("dataset")
        trainSplit, validationSplit, testSplit = datasetConfig["split"]

        random.seed = datasetConfig["seed"]
        np.random.seed(datasetConfig["seed"])

        # generating a bump signal for each dataset split
        train = self.buildBySplit(trainSplit)
        val = self.buildBySplit(validationSplit)
        test = self.buildBySplit(testSplit)

        self.project.registerData("dataset-training", train)
        self.project.registerData("dataset-validation", val)
        self.project.registerData("dataset-test", test)
