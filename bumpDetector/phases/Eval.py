from pyPhases import Phase
from pyPhasesML import DatasetWrapXY, Model, ModelManager, Scorer
from tqdm import tqdm


class Eval(Phase):
    allowTraining: bool = True

    def getModel(self) -> Model:
        modelState = self.project.getData("modelState", Model, generate=Eval.allowTraining)
        model = ModelManager.getModel(True)
        model.build()
        model.loadState(modelState)
        return model

    def main(self):
        # load or generate test data and model state
        testData = self.project.getData("dataset-test", tuple)
        model = self.getModel()

        # setup data and scorer
        testDataSet = DatasetWrapXY(testData, batchSize=model.batchSize)
        scorer = Scorer(classNames=self.getConfig("classification.classNames"), trace=True)
        # use the validation metrics also for evaluation
        scorer.metrics = self.getConfig("trainingParameter.validationMetrics")

        for data in tqdm(testDataSet):
            x, truth = data
            prediction = model.predict(x)
            scorer.score(truth, prediction)

        result = scorer.scoreAllRecords()

        print("result: %s" % str(result))
