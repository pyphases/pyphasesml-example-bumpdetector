import tensorflow as tf
from tensorflow.keras import activations, layers, models

from pyPhasesML.adapter.ModelKerasAdapter import ModelKerasAdapter


class CNNTF(ModelKerasAdapter):
    def initialOptions(self):
        return {
            "layerCount": 6,
        }

    def define(self):
        inputShape = self.inputShape
        numClasses = self.numClasses
        outputSize = self.getOption("outputSize")
        layerCount = self.getOption("layerCount")

        model = models.Sequential()

        for i in range(layerCount):
            model.add(layers.Conv1D(64, kernel_size=3, padding="same", activation=activations.relu))
            model.add(layers.MaxPooling1D(2, 2, padding="valid"))

        model.add(layers.Flatten())
        model.add(layers.Dense(256, activation=activations.relu))

        model.add(layers.Dense(outputSize))
        model.add(layers.Activation("sigmoid"))
        model.add(layers.Reshape((outputSize, 1)))

        self.model = model
        return self

    def getLossFunction(self):
        return tf.keras.losses.BinaryCrossentropy()


class MaskedBinaryCrossentropy(tf.keras.losses.BinaryCrossentropy):
    def call(self, y_true, y_pred):

        mask = y_true != -1
        y_true = y_true[mask]
        y_pred = y_pred[mask]
        return super().call(y_true, y_pred)
