from math import ceil

import torch
import torch.nn as nn
import torch.nn.functional as Functional
from torch.nn.modules.container import Sequential
from torch.nn.modules.dropout import Dropout
from torch.nn.modules.pooling import MaxPool1d

from pyPhasesML.adapter.ModelTorchAdapter import ModelTorchAdapter


class CNNLayer(nn.Module):
    def __init__(self, inChannels, outChannels, groups=1, kernelSize=3, padding=None, dilation=1, dropout=0, pool=(2, 2)):
        super().__init__()

        self.conv = Sequential(
            nn.Conv1d(
                in_channels=inChannels, out_channels=outChannels, groups=groups, kernel_size=kernelSize, dilation=dilation
            ),
            nn.ReLU(),
            Dropout(dropout),
            MaxPool1d(kernel_size=pool[0], stride=pool[1]),
            nn.BatchNorm1d(outChannels, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
        )

    def forward(self, x):
        return self.conv(x)


class CNN(nn.Module):
    def __init__(self, layerCount=6, channelCount=1, numClasses=5, width=3000, config=None):

        super().__init__()
        self.curChannels = channelCount

        self.layers = Sequential()
        self.layerCount = 0
        self.width = width
        self.outputSize = config["outputSize"]

        for i in range(layerCount):
            self.addLayer(64)

        self.dense = Sequential(
            nn.Linear(self.width * self.curChannels, 256),
            nn.ReLU(),
            nn.Linear(256, self.outputSize),
            nn.Sigmoid(),
        )

    def addLayer(self, outChannels, groups=1, kernelSize=3, padding=None, dilation=1, dropout=0, pool=(2, 2)):
        name = "layer%i" % self.layerCount
        self.layers.add_module(
            name, CNNLayer(self.curChannels, outChannels, groups, kernelSize, padding, dilation, dropout, pool)
        )
        self.layerCount += 1
        self.curChannels = outChannels
        self.width = ceil(self.width / pool[0] - kernelSize / 2)

    def forward(self, x):
        out = self.layers(x)
        _, c, w = out.shape
        out = out.view(-1, c * w)
        out = self.dense(out)
        return out

    def loss(self, output, target, batchSize, classificator):
        weightTensors = torch.IntTensor(classificator["classWeights"])
        l = torch.nn.MultiLabelSoftMarginLoss(reduction="mean", weight=weightTensors).cuda()
        output = output.permute(0, 2, 1).contiguous().squeeze()
        return l(output, target.squeeze()) * classificator["weight"], 0

    def remapOutputsToPrediction(self, outputs):
        pred = []
        for i, classCount in enumerate(self.binClasses):
            pred.append(outputs[i].data.cpu().permute(0, 2, 1).contiguous().view(-1, classCount))
        return pred


class CNNPytorch(ModelTorchAdapter):
    def initialOptions(self):
        return {
            "layerCount": 6,
            "outputSize": 6,
            "downsampel": [2],
        }

    def define(self):
        inputShape = self.inputShape
        numClasses = self.numClasses
        self.model = CNN(
            layerCount=self.getOption("layerCount"),
            channelCount=inputShape[1],
            numClasses=numClasses,
            width=inputShape[0],
            config=self.options,
        )

        self.weightTensors = None if self.classWeights is None else torch.IntTensor(self.classWeights)
        if self.useGPU:
            self.model.cuda()

    def getLossFunction(self):
        loss = torch.nn.MultiLabelSoftMarginLoss(reduction="mean", weight=self.weightTensors)
        if self.useGPU:
            loss.cuda()
        return loss

    def mapOutput(self, output):
        return output

    def mapOutputForLoss(self, output, mask):
        return output.reshape(-1)

    def mapOutputForPrediction(self, output):
        return output
