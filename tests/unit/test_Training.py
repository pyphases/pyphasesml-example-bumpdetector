from pathlib import Path
from pyPhasesML import ModelManager
from unittest.mock import MagicMock

from pyPhases.test.TestCase import TestCase

from bumpDetector.phases.Training import Training


class TestTraining(TestCase):
    phase = Training()

    def config(self):
        return {
            "dataset": {"split": [10, 3, 8], "segmentlength": 60, "seed": 2, "noise": 0.1, "bumpLikelyhood": 0.5},
            "modelName": "testModel",
            "model": {"myModelOption": 1},
        }

    def data(self):
        return {
            "dataset-training": ([0, 1, 2, 3], [0, 0, 1, 1]),
            "dataset-validation": ([3, 4], [0, 1]),
        }

    def beforePrepare(self):
        pass

    def testDerivedInputShape(self):
        self.assertEqual(self.getConfig("inputShape"), (60, 1))

    def testFolderCreation(self):

        Path.exists = MagicMock(return_value=False)
        Path.mkdir = MagicMock()

        self.phase.createLogFolder("testFolder")
        Path.exists.assert_called_once()
        Path.mkdir.assert_called_once()

    def testMain(self):
        self.modelMock = MagicMock()
        ModelManager.getModel = MagicMock(return_value=self.modelMock)
        self.phase.createLogFolder = MagicMock()
        self.project.saveConfig = MagicMock()
        self.modelMock.train = MagicMock(return_value="myModel")

        self.phase.main()

        # create log folder
        self.phase.createLogFolder.assert_called_once()

        # training was run
        self.modelMock.train.assert_called_once()

        # the trainingdata is equal to the data from the dataset
        trainData = self.modelMock.train.call_args[0][0]

        assert trainData.trainingData.x == [0, 1, 2, 3]
        assert trainData.trainingData.y == [0, 0, 1, 1]
        assert trainData.validationData.x == [3, 4]
        assert trainData.validationData.y == [0, 1]

        # save config after training
        self.project.saveConfig.assert_called_once()

        # model was exported
        self.assertDataEqual("modelState", "myModel")
