import numpy as np

from unittest.mock import MagicMock, call, patch
from bumpDetector.phases.Eval import Eval, Scorer

from pyPhases.test.TestCase import TestCase
from pyPhasesML import ModelManager


class TestEval(TestCase):
    phase = Eval()

    def config(self):
        # self.setConfig("trainingParameter.validationMetrics", ["myMetric"])
        return {
            "dataset": {"split": [10, 3, 8], "segmentlength": 60, "seed": 2, "noise": 0.1, "bumpLikelyhood": 0.5},
            "modelName": "testModel",
            "model": {"myModelOption": 1},
            "classification": {"classNames": ["C0", "C1"]},
            "trainingParameter.validationMetrics": ["myMetric"],
        }

    def data(self):
        return {
            # X: [0, 1], [2, 3], [4, 5]
            # Y: [6, 7], [8, 9], [10, 11]
            "dataset-test": np.arange(12).reshape(2, 3, 2).tolist(),
            "modelState": "MyModelState",
        }

    def testGetModel(self):
        modelMock = MagicMock()
        ModelManager.getModel = MagicMock(return_value=modelMock)

        # check if the saved state is loaded
        m = self.phase.getModel()
        modelMock.loadState.assert_called_with("MyModelState")

        # model should be returned
        assert m == modelMock

    def testMain(self):
        scorerMock = MagicMock(return_value=None)
        with patch.object(Scorer, "__init__", scorerMock):
            modelMock = MagicMock()
            predictionMock = MagicMock(return_value="prediction")
            modelMock.predict = predictionMock
            modelMock.batchSize = 1
            ModelManager.getModel = MagicMock(return_value=modelMock)

            Scorer.scoreAllRecords = MagicMock()
            Scorer.score = MagicMock()

            self.phase.main()
            scorerMock.assert_called_once()
            scorerMock.assert_called_with(classNames=["C0", "C1"], trace=True)

            assert self.getConfig("trainingParameter.validationMetrics") == ["myMetric"]

            self.assertEqual(modelMock.predict.call_count, 3)

            # check if x from dataset is loaded correctly and prediction is called
            modelMock.predict.assert_has_calls(
                [
                    call([[0, 1]]),
                    call([[2, 3]]),
                    call([[4, 5]]),
                ]
            )

            # check if y from dataset is loaded correctly and scored with predction
            Scorer.score.assert_has_calls(
                [
                    call([[6, 7]], "prediction"),
                    call([[8, 9]], "prediction"),
                    call([[10, 11]], "prediction"),
                ]
            )

            Scorer.scoreAllRecords.assert_called_once()
