from unittest.mock import MagicMock

from pyPhases.test.TestCase import TestCase

from bumpDetector.phases.BuildDataset import BuildDataset

import numpy.testing as npt


class TestBuildDataset(TestCase):
    phase = BuildDataset()

    def config(self):
        return {"dataset": {"split": [10, 3, 8], "segmentlength": 60, "seed": 2, "noise": 0.1, "bumpLikelyhood": 0.5}}

    def testDatasetShape(self):
        bumpsSignal, validationSignal = self.phase.generateBumpSignal(100, 10)

        npt.assert_equal(bumpsSignal.shape, (10, 100, 1))
        npt.assert_equal(validationSignal.shape, (10, 100))

    def testSplitShapeWithConfig(self):

        for splitSize in range(70, 80, 85):
            x, y = self.phase.buildBySplit(splitSize)
            npt.assert_equal(x.shape, (splitSize, 60, 1))
            npt.assert_equal(y.shape, (splitSize, 60))

    def testBuildingDatasets(self):
        self.phase.main()

        trainX, trainY = self.getData("dataset-training")
        valX, valY = self.getData("dataset-validation")
        testX, testY = self.getData("dataset-test")

        npt.assert_equal(trainX.shape, (10, 60, 1))
        npt.assert_equal(valX.shape, (3, 60, 1))
        npt.assert_equal(testX.shape, (8, 60, 1))

        npt.assert_equal(trainY.shape, (10, 60))
        npt.assert_equal(valY.shape, (3, 60))
        npt.assert_equal(testY.shape, (8, 60))
