# Bump Detector

This is a small example of using [pyPhases](https://pypi.org/project/pyPhases/) for machine learning projects.


## Running examples

### Gitpod

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/tud.ibmt.public/pyphases/pyphasesml-example-bumpdetector)

Run: `phases run Training`

### Docker

`docker run -v ./data:/app/data -v ./logs:/app/logs registry.gitlab.com/tud.ibmt.public/pyphases/pyphasesml-example-bumpdetector/torch:latest run Training`


### Local setup

-  Python >= `3.5`
-  A supported machine learning framework ([PyTorch](https://pytorch.org/get-started/locally/) or [TensorFlow](https://www.tensorflow.org/install))
-  Create a data folder `mkdir data`
- Install requirements: `pip install -r requirements.txt`
- Run Training: `phases run Training`


## Training

### PyTorch

`phases run Training`

### Tensorflow

`phases run --set modelName=CNNTF Training`

### load additional config

`phases run -c myConfig.yaml Training`

## Development

### Run Tests

`phases test tests`

## pyPhase structure
| Complete                           | Training with pyPhases                               |
|------------------------------------|------------------------------------------------------|
| ![Structure](assets/structure.svg) | ![PyPhases-Structure](assets/structure-pyPhases.svg) |

