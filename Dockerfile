######################################################################################################
# PyTorch Build
######################################################################################################

FROM pytorch/pytorch:1.13.1-cuda11.6-cudnn8-runtime as prod-torch

ENV VERSION="0.0.0"

WORKDIR /app

COPY requirements.txt .
COPY bumpDetector bumpDetector
COPY configs configs
COPY project*.yaml ./

RUN echo $VERSION > ./VERSION && \
    pip install --upgrade pip setuptools wheel && \
    mkdir data


RUN pip install --user -r requirements.txt

ENTRYPOINT ["python", "-m", "phases"]

######################################################################################################
# Tensorlfow Build
######################################################################################################

FROM tensorflow/tensorflow:2.4.2-gpu as prod-tensorflow

ENV VERSION="0.0.0"
ENV distro="ubuntu2004"
ENV arch="x86_64"

WORKDIR /app

COPY requirements.txt .
COPY bumpDetector bumpDetector
COPY configs configs
COPY project*.yaml ./

RUN apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/$distro/$arch/3bf863cc.pub
RUN echo $VERSION > ./VERSION && \
    apt-get -y update && \
    pip install --upgrade pip setuptools wheel && \
    mkdir data


RUN pip install --user -r requirements.txt

ENTRYPOINT ["python", "-m", "phases"]


######################################################################################################
# Build for automatic tests (uses torch)
######################################################################################################

FROM prod-torch as prod-test

RUN pip install --user coverage


######################################################################################################
# Tensorflow ROCM builds for AMD GPU
######################################################################################################

# FROM rocm/tensorflow:rocm3.3-tf2.2-rc2-dev as prod-rocm

# EXPOSE 5000
# ENV VERSION="0.0.0"
# WORKDIR /app

# COPY bumpDetector bumpDetector

# RUN add-apt-repository -y ppa:deadsnakes/ppa && \
#     apt-get -y update && apt-get install -y \
#     rocm-libs \
#     hipcub \
#     miopen-hip \
#     graphviz \
#     git \
#     python3.7 \
#     python3.7-distutils && \
#     apt-get remove -y python3-pip && \
#     python3.7 -m easy_install pip \
#     && apt-get remove -y python-setuptools && \
#     python3.7 -m pip install -U pip setuptools

# COPY requirements.txt .

# RUN python3.7 -m pip install --user tensorflow-rocm --upgrade && \
#     usermod -a -G video root && \
#     python3.7 -m pip install -r requirements.txt

# ENTRYPOINT ["python3.7", "-m", "phases",]

FROM python:3.11.2-slim as test

WORKDIR /app

COPY requirements.dev.txt requirements.dev.txt
COPY tests tests
COPY requirements.txt .
COPY bumpDetector bumpDetector
COPY configs configs
COPY project*.yaml ./

RUN apk add alpine-sdk && \
    pip install --upgrade pip setuptools wheel && \
    mkdir data

RUN pip install --user -r requirements.txt && pip install --user -r requirements.dev.txt

ENTRYPOINT ["python", "-m", "phases"]